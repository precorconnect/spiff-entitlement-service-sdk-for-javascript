import {inject} from 'aurelia-dependency-injection';
import SpiffEntitlementServiceSdkConfig from './spiffEntitlementServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';

@inject(SpiffEntitlementServiceSdkConfig, HttpClient)
class UpdatePartnerRepFeature {

    _config:SpiffEntitlementServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:SpiffEntitlementServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Update the partner rep
     * @param {number} partnerSaleRegistrationId
     * @param {string} partnerRepUserId
     * @param {string} accessToken
     * @returns {Promise.<void>}
     */
    execute(partnerSaleRegistrationId:number,
            partnerRepUserId:string,
            accessToken:string):Promise<void> {

        return this._httpClient
            .createRequest(`spiff-entitlements/${partnerSaleRegistrationId}/partnerrepuserid/${partnerRepUserId}`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .catch(error => {
                console.log(error);
            })
            .then(response => response);
    }
}

export default UpdatePartnerRepFeature;