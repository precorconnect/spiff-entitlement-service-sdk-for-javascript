import SpiffEntitlementServiceSdk from '../../src/index';
import config from './config';
import factory from './factory';
import dummy from '../dummy';

describe('Index module', () => {

    describe('default export', () => {
        it('should be SpiffEntitlementServiceSdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new SpiffEntitlementServiceSdk(config.spiffEntitlementServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(SpiffEntitlementServiceSdk));

        });
    });

        describe('instance of default export', () => {

        describe('updatePartnerRep method', () => {
            it('should update the partnerrepuserid, return type is void', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new SpiffEntitlementServiceSdk(config.spiffEntitlementServiceSdkConfig);

                /*
                 act
                 */
               // const actPromise =
                    objectUnderTest
                        .updatePartnerRep(
                            dummy.partnerSaleRegistrationId,
                            dummy.partnerRepUserId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        )

            },20000);
        });


    });
});